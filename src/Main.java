
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Node nA=new Node('A');
		Node nD=new Node('D');
		Node nB=new Node('B');
		Node nE=new Node('E');
		Node nC=new Node('C');

		Graph g=new Graph();
		g.addNode(nA);
		g.addNode(nD);
		g.addNode(nB);
		g.addNode(nE);
		g.addNode(nC);
		g.setRootNode(nA);
		
		g.connectNode(nA,nD);
		
		g.connectNode(nD,nB);
		g.connectNode(nD,nE);
		
		g.connectNode(nB,nE);
		
		g.connectNode(nE,nC);
				
		System.out.println("Busca em profundidade");
		g.dfs();
	}

}
