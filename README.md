# **DFS** #

### **Busca em Profundidade** ###

Algoritmo adaptado do site Code Project.
[http://www.codeproject.com/Articles/32212/Introduction-to-Graph-with-Breadth-First-Search-BF](http://www.codeproject.com/Articles/32212/Introduction-to-Graph-with-Breadth-First-Search-BF)

Alunos: Marcos Batista e Silas Gonçalves

Professor Msc: Fernando Luiz

Disciplina: Inteligência Artificial 2014/2